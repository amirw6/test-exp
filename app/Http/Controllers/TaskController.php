<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User; 
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;



class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::id();
        $tasks = Task::all();        
        return view('tasks.index', ['tasks'=>$tasks],['id'=>$id]);


    }
    public function personal()
    {
        $id=Auth::id();
        $tasks = User::find($id)->tasks;        
        return view('tasks.personal',['tasks'=>$tasks],['id'=>$id]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request)
    {
        $task = new Task();
        $id=Auth::id();
        $task->title = $request->title;
        $task->user_id = $id;
        $task->save();
        return redirect('tasks');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit', compact('task'));
    }

    public function supdate($id)
    {
        $task = Task::find($id);
        $task->status=1;
        $task->update();
        return redirect('tasks');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $task = Task::find($id);
        $task -> update($request->all());
        return redirect('tasks');

    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            if (Gate::denies('admin')) {
                abort(403,"Sorry you are not allowed to delete tasks..");
            } 
    
            $task = Task::find($id);
            $task->delete();
            return redirect('tasks');
        
    
    }
}
