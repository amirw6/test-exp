<h1>Create New task</h1>
<form method = 'post' action="{{action('TaskController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">What is your new task?</label>
    <input type= "text" class = "form-control" name= "title">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

</form>
